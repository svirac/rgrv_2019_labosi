# Plane calculator adapted from: https://www.geeksforgeeks.org/program-to-find-equation-of-a-plane-passing-through-3-points/

import numpy as np
import math
import cv2 as cv
import random
import math


def main():

    # Original image
    imgNum = input('Choose picture [1-9]: ')
    imgPath = "KinectPics/sl-0000" + imgNum + ".bmp"
    imgRGB = cv.imread(imgPath)
    cv.imshow('Original image', imgRGB)
    cv.waitKey()
    #cv.destroyAllWindows()

    # Depth image
    imgDepth = imgRGB.copy()
    imgDepth_gray = cv.cvtColor(imgDepth, cv.COLOR_BGR2GRAY)
    point3DArray, n3DPoints, imgDepth_gray = ReadKinectPic(imgPath, imgDepth_gray)
    imgDepth_RGB = cv.cvtColor(imgDepth_gray, cv.COLOR_GRAY2BGR)
    cv.imshow('Depth image', imgDepth_RGB)
    cv.waitKey()
    #cv.destroyAllWindows()


    ############################################  RANSAC algorithm start  ##############################################

    iterations = 300
    threshold = 4

    best_plane_point_count = 0
    best_plane_points = []

    loading_time = 0
    iter_percent = int(round(iterations/100))

    for i in range(iterations):

        # Show iteration percentage
        if (i % iter_percent) == 0:
            print('Proccessing image! [%d%%]\r' % loading_time, end="")
            loading_time += 1

        # Pick three random points
        while True:
            pt1 = random.choice(point3DArray)
            pt2 = random.choice(point3DArray)
            pt3 = random.choice(point3DArray)
            if pt1 != pt2 and pt2 != pt3 and pt1 != pt3:
                break

        # Plane equation: ax + by + cz + d = 0
        a, b, c, d = PlaneCalculator(pt1, pt2, pt3)

        # Find a plane with the most points
        plane_point_count = 0
        plane_points = []
        for j in range(n3DPoints):
            if IsOnPlane(point3DArray[j], a, b, c, d, threshold):
                plane_point_count += 1
                plane_points.append(j)

        if (plane_point_count > best_plane_point_count):
            best_plane_point_count = plane_point_count
            best_plane_points = plane_points

    ########################################  RANSAC algorithm  end  ###################################################


    plane_equation = GetPlaneEquation(a, b, c, d)
    print("Plane equation: ", plane_equation)
    print("Plane contains", best_plane_point_count, "points.")

    # Color best plane points in green
    imgDepth_dominant = imgDepth_RGB.copy()
    for i in best_plane_points:
        imgDepth_dominant[(point3DArray[i])[1], (point3DArray[i])[0]] = (0, 255, 0)

    # Depth image with dominant plane
    cv.imshow('Dominant plane', imgDepth_dominant)
    cv.waitKey()
    cv.destroyAllWindows()


# Function takes three points
# Returns plane coefficients
def PlaneCalculator(pt1, pt2, pt3):

    a1 = pt2[0] - pt1[0]
    b1 = pt2[1] - pt1[1]
    c1 = pt2[2] - pt1[2]
    a2 = pt3[0] - pt1[0]
    b2 = pt3[1] - pt1[1]
    c2 = pt3[2] - pt1[2]

    a = b1*c2 - b2*c1
    b = a2*c1 - a1*c2
    c = a1*b2 - b1*a1
    d = (-a*pt1[0] - b*pt1[1] - c*pt1[2])

    return a, b, c, d


# Function takes point, plane coefficients and threshold
# Returns true if distance between point and plane is less than threshold
def IsOnPlane(pt, a, b, c, d, threshold):

    num = abs(pt[0]*a + pt[1]*b + pt[2]*c + d)
    den = math.sqrt(a*a + b*b + c*c)
    distance = num/den
    return distance <= threshold


def GetPlaneEquation(a, b, c, d):

    a_str = str(a)
    b_str = str(b)
    c_str = str(c)
    d_str = str(d)

    plane_equation = a_str

    if(b > 0):
        plane_equation += "x + " + b_str
    else:
        plane_equation += "x - " + b_str[1:]

    if(c > 0):
        plane_equation += "y + " + c_str
    else:
        plane_equation += "y - " + c_str[1:]

    if(d > 0):
        plane_equation += "z + " + d_str + " = 0"
    else:
        plane_equation += "z - " + d_str[1:] + " = 0"

    return plane_equation


def ReadKinectPic(pathRGB, depthImage):

    DepthMap = [None] * (len(depthImage) * len(depthImage[0]))

    dmin = 2047
    dmax = 0

    point3DArray= []

    # Get DepthImage file path
    pathDepth = pathRGB[:-4]
    pathDepth = pathDepth + "-D.txt"

    fp = open(pathDepth, 'r')
    data = fp.read()
    data = data.split(' ')
    data = [i.replace('\n', '') for i in data]
    data.remove('')

    d_list = []
    for element in data:
        d_list.append(int(element))

    cnt = 0

    if fp:
        for idxv in range(len(depthImage)):
            for idxu in range(len(depthImage[0])):

                d = d_list[cnt]

                if d == 2047:
                    d = 0

                else:
                    if d < dmin:
                        dmin = d
                    if d > dmax:
                        dmax = d

                DepthMap[idxv * len(depthImage[0]) + idxu] = d
                point3DArray.append([idxu, idxv, d])
                cnt += 1

                if cnt==76801:
                    break

    fp.close()
    n3DPoints = len(point3DArray)

    for idxv in range(len(depthImage)):
        for idxu in range(len(depthImage[0])):

            d = DepthMap[idxv * len(depthImage[0]) + idxu]

            if d != -1:
                d = ((d - dmin) * 254 / (dmax - dmin)) + 1
            else:
                d = 0

            depthImage[idxv][idxu] = d

    return point3DArray, n3DPoints, depthImage


if __name__=='__main__':
    main()