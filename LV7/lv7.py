import vtk
import numpy as np
import time
import matplotlib.pyplot as plt
import numpy as np
from decimal import Decimal

pathSource = 'Models/bunny_t3.ply'
pathDestination = 'Models/bunny.ply'

landmarks = [10, 50, 100, 500, 1000]
iterations = [10, 50, 100, 500, 1000]

MSE = np.array([])
ICPtime = np.array([])

for iteration in iterations:
    MSE_row = np.array([])
    ICPtime_row = np.array([])
    for landmark in landmarks:

        # Renderer, renderer window, and renderer window interactor 1
        renderer1 = vtk.vtkRenderer()
        renderer1.SetBackground(1, 1, 1)

        renderWindow1 = vtk.vtkRenderWindow()
        renderWindow1.AddRenderer(renderer1)
        renderWindow1.SetSize(800, 600)
        renderWindow1.SetWindowName('Original position')

        renderWindowIndowinteractor1 = vtk.vtkRenderWindowInteractor()
        renderWindowIndowinteractor1.SetRenderWindow(renderWindow1)

        # Renderer, renderer window, and renderer window interactor 2
        renderer2 = vtk.vtkRenderer()
        renderer2.SetBackground(1, 1, 1)

        renderWindow2 = vtk.vtkRenderWindow()
        renderWindow2.AddRenderer(renderer2)
        renderWindow2.SetSize(800, 600)
        renderWindow2.SetWindowName("Window 2")

        renderWindowIndowinteractor2 = vtk.vtkRenderWindowInteractor()
        renderWindowIndowinteractor2.SetRenderWindow(renderWindow2)


        # Read models
        source = vtk.vtkPolyData()
        plyReaderSource = vtk.vtkPLYReader()
        plyReaderSource.SetFileName(pathSource)
        plyReaderSource.Update()
        source = plyReaderSource.GetOutput()

        destination = vtk.vtkPolyData()
        plyReaderDestination = vtk.vtkPLYReader()
        plyReaderDestination.SetFileName(pathDestination)
        plyReaderDestination.Update()
        destination = plyReaderDestination.GetOutput()


        # Mapping
        mapperSource = vtk.vtkPolyDataMapper()
        mapperSource.SetInputData(source)
        actSource = vtk.vtkActor()
        actSource.SetMapper(mapperSource)
        actSource.GetProperty().SetColor(0, 0, 1)
        renderer1.AddActor(actSource)

        mapperDestination = vtk.vtkPolyDataMapper()
        mapperDestination.SetInputData(destination)
        actDestination = vtk.vtkActor()
        actDestination.SetMapper(mapperDestination)
        actDestination.GetProperty().SetColor(0, 1, 0)
        renderer1.AddActor(actDestination)
        renderer2.AddActor(actDestination)


        # ICP algorithm start ########################################
        ICP = vtk.vtkIterativeClosestPointTransform()
        ICP.SetSource(source)
        ICP.SetTarget(destination)
        ICP.GetLandmarkTransform().SetModeToRigidBody()
        ICP.SetMaximumNumberOfIterations(iteration)
        ICP.SetMaximumNumberOfLandmarks(landmark)

        # Measure time needed for ICP
        start = time.time()
        ICP.Update()
        end = time.time()

        ICPtime_row = np.append(ICPtime_row, end-start)

        # Add transformed source object on renderer2 to match the destination
        transformFilter = vtk.vtkTransformPolyDataFilter()
        transformFilter.SetInputData(source)
        transformFilter.SetTransform(ICP)
        transformFilter.Update()

        mapperTransform = vtk.vtkPolyDataMapper()
        mapperTransform.SetInputConnection(transformFilter.GetOutputPort())

        actTranform = vtk.vtkActor()
        actTranform.SetMapper(mapperTransform)
        actTranform.GetProperty().SetColor(0, 0, 1)
        renderer2.AddActor(actTranform)

        # MSE calculation
        dataTrans = transformFilter.GetOutput().GetPoints().GetData()
        dataDest = destination.GetPoints().GetData()
        sum = 0
        for i in range(dataTrans.GetNumberOfTuples()):
            x1 = dataTrans.GetTuple3(i)[0]
            y1 = dataTrans.GetTuple3(i)[1]
            z1 = dataTrans.GetTuple3(i)[2]
            x2 = dataDest.GetTuple3(i)[0]
            y2 = dataDest.GetTuple3(i)[1]
            z2 = dataDest.GetTuple3(i)[2]

            distance = (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2
            sum = sum + distance

        currError = sum / dataTrans.GetNumberOfTuples()

        MSE_row = np.append(MSE_row, currError)

        # if iteration == 500 and landmark == 500:
        #     renderWindow1.Render()
        #     renderWindow2.Render()
        #     renderWindow1.SetWindowName('Original position')
        #     renderWindow2.SetWindowName('Transformed position')
        #     renderWindowIndowinteractor1.Start()
        #     renderWindowIndowinteractor2.Start()

    # Update MSE array
    if MSE.size == 0:
        MSE = MSE_row
    else:
        MSE = np.vstack([MSE, MSE_row])

    # Update ICPtime array
    if ICPtime.size == 0:
        ICPtime = ICPtime_row
    else:
        ICPtime = np.vstack([ICPtime, ICPtime_row])



# Format ICPtime to 3 decimal places
ICPtime_formatted = np.round(ICPtime, 3)

# Array of MSE and ICP time to show in table
MSE_ICPtime = np.array([])
for i in range(5):
    row = np.array([])
    for j in range(5):
        x = '%.3e' % Decimal(MSE[i][j])
        row = np.append(row, ['MSE: ' + x + '\nICP time: ' + str(ICPtime_formatted[i][j])])

    if(MSE_ICPtime.size == 0):
        MSE_ICPtime = row
    else:
        MSE_ICPtime = np.vstack([MSE_ICPtime, row])


# Table
fig = plt.figure(figsize=(16, 6))
ax = fig.add_subplot(1,1,1)

labelColour = ['#E67E22']*5
cellColour = [['#F0B27A']*5, ['#FAD7A0']*5, ['#F0B27A']*5, ['#FAD7A0']*5, ['#F0B27A']*5]

table = ax.table(cellText=MSE_ICPtime, loc='center',
                 rowLabels=['10 iterations', '50 iterations', '100 iterations', '500 iterations', '1000 iterations'],
                 colLabels=['10 landmarks', '50 landmarks', '100 landmarks', '500 landmarks', '1000 landmarks'],
                 rowColours=labelColour, colColours=labelColour,
                 cellColours=cellColour)

table.scale(1,3)
table.set_fontsize(14)
ax.axis('off')
fig.suptitle('MSE and ICP time for iteration and landmark combinations', fontsize=20)
plt.show()
