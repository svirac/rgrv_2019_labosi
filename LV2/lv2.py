# Adapted from:
# https://www.learnopencv.com/delaunay-triangulation-and-voronoi-diagram-using-opencv-c-python/
# https://stackoverflow.com/questions/50403176/mouse-events-on-opencv

import numpy as np
import cv2 as cv
from random import seed
import random


# Removes points that are outside of the rectangle
def rect_contains(rect, point):
    
    if point[0] < rect[0]:
        return False
    elif point[1] < rect[1]:
        return False
    elif point[0] > rect[2]:
        return False
    elif point[1] > rect[3]:
        return False
    return True


def draw_delaunay(img, subdiv, delaunay_color):
    
    triangleList = subdiv.getTriangleList()
    size = img.shape
    r = (0, 0, size[0], size[1])
 
    for t in triangleList:
         
        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])
         
        if rect_contains(r, pt1) and rect_contains(r, pt2) and rect_contains(r, pt3):
            
            cv.line(img, pt1, pt2, delaunay_color, thickness = 1)
            cv.line(img, pt2, pt3, delaunay_color, thickness = 1)
            cv.line(img, pt3, pt1, delaunay_color, thickness = 1)
            

# Returns triangle for given edge or empty np.array
def getTriangle(edge1):
    
    size = img.shape
    r = (0, 0, size[0], size[1])
    
    # Point1
    vertex1 = subdiv.edgeOrg(edge1)[0]
    point1 = tuple(map(int, subdiv.getVertex(vertex1)[0]))
    
    # Point2
    edge2 = subdiv.getEdge(edge1, cv.SUBDIV2D_NEXT_AROUND_LEFT)
    vertex2 = subdiv.edgeOrg(edge2)[0]
    point2 = tuple(map(int, subdiv.getVertex(vertex2)[0]))
    
    # Point3
    edge3 = subdiv.getEdge(edge2, cv.SUBDIV2D_NEXT_AROUND_LEFT)
    vertex3 = subdiv.edgeOrg(edge3)[0]
    point3 = tuple(map(int, subdiv.getVertex(vertex3)[0]))
    
    triangle = np.array([point1, point2, point3])  
    
    if rect_contains(r, point1) and rect_contains(r, point2) and rect_contains(r, point3):
        return triangle
    else:
        return np.array([[0, 0],[0, 0],[0, 0]])


# Function returns neighbour triangles for given edge
def getNeighbours(edge1):
    
    edge1_rot = subdiv.rotateEdge(edge1, 2)
    neighbour1 = getTriangle(edge1_rot)

    edge2 = subdiv.getEdge(edge1, cv.SUBDIV2D_NEXT_AROUND_LEFT)
    edge2_rot = subdiv.rotateEdge(edge2, 2)
    neighbour2 = getTriangle(edge2_rot)

    edge3 = subdiv.getEdge(edge2, cv.SUBDIV2D_NEXT_AROUND_LEFT)
    edge3_rot = subdiv.rotateEdge(edge3, 2)
    neighbour3 = getTriangle(edge3_rot)
    
    return neighbour1, neighbour2, neighbour3


# Function activates on MouseCallback and colors selected triangle and it's neigbhbour triangles
def colorTriangle(event, x, y, flags, subdiv):
    
    edge1 = subdiv.locate((x,y))[1]
    triangle = getTriangle(edge1)

    if(np.all(triangle)):
        
        neighbour1, neighbour2, neighbour3 = getNeighbours(edge1)
        neighbours = [neighbour1, neighbour2, neighbour3]
        
        if event == cv.EVENT_LBUTTONDOWN:
            cv.fillPoly(img, pts=[triangle],color=(0,0,255))
            for neighbour in neighbours:
                cv.fillPoly(img, pts=[neighbour],color=(255,0,0))
            draw_delaunay(img, subdiv, (0,0,0))
            
        elif event == cv.EVENT_LBUTTONUP:
            cv.fillPoly(img, pts=[triangle],color=(255,255,255))
            for neighbour in neighbours:
                cv.fillPoly(img, pts=[neighbour],color=(255,255,255))
            draw_delaunay(img, subdiv, (0,0,0))
        
    else:
        draw_delaunay(img, subdiv, (0,0,0))
    

# Generate points
seed(120)
points = []
for i in range(0, 20):
    x = random.randrange(20, 420)
    y = random.randrange(20, 420)
    points.append((x,y))
    
# Define image
img = np.empty((440, 440, 3))
img.fill(255)
size = img.shape
rect = (0, 0, size[0], size[1])

# Define subdiv
subdiv = cv.Subdiv2D(rect)
for p in points:
    subdiv.insert(p)
 
draw_delaunay(img, subdiv, (0,0,0))

# Define clickable window
windowName = 'Delaunay triangulation'
cv.namedWindow(windowName)
cv.setMouseCallback(windowName, colorTriangle, subdiv)

while True:
    cv.imshow(windowName, img)
    if cv.waitKey(1) == 27:    #Break on ESC key
        break
    
cv.destroyAllWindows()