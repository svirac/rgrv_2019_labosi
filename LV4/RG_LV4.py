import cv2 as cv
import numpy as np

img1 = cv.imread('ImageT0.jpg', cv.IMREAD_GRAYSCALE)
img_scene = cv.imread('ImageT2.jpg', cv.IMREAD_GRAYSCALE)

fromCenter = False
r = cv.selectROI('Image', img1, fromCenter)
img_obj = img1[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
cv.destroyAllWindows()

sift = cv.xfeatures2d.SIFT_create()

keypoints_obj, descriptors_obj = sift.detectAndCompute(img_obj, None)
keypoints_scene, descriptors_scene = sift.detectAndCompute(img_scene, None)

img_obj_keypoints = np.empty((img_obj.shape[0], img_obj.shape[1], 3), dtype=np.uint8)
cv.drawKeypoints(img_obj, keypoints_obj, img_obj_keypoints)

img_scene_keypoints = np.empty((img_scene.shape[0], img_scene.shape[1], 3), dtype=np.uint8)
cv.drawKeypoints(img_scene, keypoints_scene, img_scene_keypoints)

img_obj_keypoints_resize = cv.copyMakeBorder(img_obj_keypoints,img_scene.shape[0]-img_obj_keypoints.shape[0],0,img_scene.shape[1]-img_obj_keypoints.shape[1],0,cv.BORDER_CONSTANT,value=[255,255,255])
img_keypoints = np.hstack((img_obj_keypoints_resize, img_scene_keypoints))

cv.imshow('SIFT Keypoints', img_keypoints)
cv.waitKey()
cv.destroyAllWindows()


#----------------------------------------------------


matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_FLANNBASED)
knn_matches = matcher.knnMatch(descriptors_obj, descriptors_scene, 2)
#-- Filter matches using the Lowe's ratio test
ratio_thresh = 0.75
good_matches = []
for m,n in knn_matches:
    if m.distance < ratio_thresh * n.distance:
        good_matches.append(m)
#-- Draw matches
img_matches = np.empty((max(img_obj.shape[0], img_scene.shape[0]), img_obj.shape[1]+img_scene.shape[1], 3), dtype=np.uint8)
cv.drawMatches(img_obj, keypoints_obj, img_scene, keypoints_scene, good_matches, img_matches, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
#-- Localize the object
obj = np.empty((len(good_matches),2), dtype=np.float32)
scene = np.empty((len(good_matches),2), dtype=np.float32)
for i in range(len(good_matches)):
    #-- Get the keypoints from the good matches
    obj[i,0] = keypoints_obj[good_matches[i].queryIdx].pt[0]
    obj[i,1] = keypoints_obj[good_matches[i].queryIdx].pt[1]
    scene[i,0] = keypoints_scene[good_matches[i].trainIdx].pt[0]
    scene[i,1] = keypoints_scene[good_matches[i].trainIdx].pt[1]
H, _ =  cv.findHomography(obj, scene, cv.RANSAC)
#-- Get the corners from the image_1 ( the object to be "detected" )
obj_corners = np.empty((4,1,2), dtype=np.float32)
obj_corners[0,0,0] = 0
obj_corners[0,0,1] = 0
obj_corners[1,0,0] = img_obj.shape[1]
obj_corners[1,0,1] = 0
obj_corners[2,0,0] = img_obj.shape[1]
obj_corners[2,0,1] = img_obj.shape[0]
obj_corners[3,0,0] = 0
obj_corners[3,0,1] = img_obj.shape[0]
scene_corners = cv.perspectiveTransform(obj_corners, H)
#-- Draw lines between the corners (the mapped object in the scene - image_2 )
cv.line(img_matches, (int(scene_corners[0,0,0] + img_obj.shape[1]), int(scene_corners[0,0,1])),\
    (int(scene_corners[1,0,0] + img_obj.shape[1]), int(scene_corners[1,0,1])), (0,255,0), 4)
cv.line(img_matches, (int(scene_corners[1,0,0] + img_obj.shape[1]), int(scene_corners[1,0,1])),\
    (int(scene_corners[2,0,0] + img_obj.shape[1]), int(scene_corners[2,0,1])), (0,255,0), 4)
cv.line(img_matches, (int(scene_corners[2,0,0] + img_obj.shape[1]), int(scene_corners[2,0,1])),\
    (int(scene_corners[3,0,0] + img_obj.shape[1]), int(scene_corners[3,0,1])), (0,255,0), 4)
cv.line(img_matches, (int(scene_corners[3,0,0] + img_obj.shape[1]), int(scene_corners[3,0,1])),\
    (int(scene_corners[0,0,0] + img_obj.shape[1]), int(scene_corners[0,0,1])), (0,255,0), 4)

cv.imshow('Good Matches & Object detection', img_matches)
cv.waitKey()
cv.destroyAllWindows()

